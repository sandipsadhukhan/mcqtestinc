/* BENGAL COLLEGE OF ENGINEERING AND TECHNOLOGY
Project Name : Program to evaluate responses to a multiple choice test.
See the latest release in https://gitlab.com/sandipsadhukhan/mcqtestinc
Teacher - Joyjit Patra
Subject - Design & Analysis of Algorithm
Group Members - Sandip Sadhukhan (87), Pranab Pal (65), Tathagata Bandyopadhyay (120),
                Sayandeep Barai (94), Rameez Raza Riaz (72)
Semester - 4th
Year - 2nd
*/
#include <stdio.h>
void main()
{
    // global vars
    int totalPoints = 0;
    int eachPoint = 2;
    int totalQuestion = 10;

    char *questions[] = {
        "The word ______ comes from the name of a Persian mathematician Abu Ja’far Mohammed ibn-i Musa al Khowarizmi",
        "Which of the following is not a stable sorting algorithm in its typical implementation.",
        "This characteristic often draws the line between what is feasible and what is impossible.",
        "When an algorithm is written in the form of a programming language, it becomes a ______. ",
        "A system wherein items are added from one and removed from the other end.",
        " Another name for 1-D arrays.",
        "Predict output of following program.\n#include <stdio.h>\n\nint fun(int n)\n{\n\tif (n == 4)\n\treturn n;\n\telse return 2*fun(n+1);\n}\n\nint main()\n{\n\tprintf(\"%d\", fun(2));\n\treturn 0;\n}",
        "Consider the following recursive function fun(x, y). What is the value of fun(4, 3)\nint fun(int x, int y) \n{\n\tif (x == 0)\n\t  return y;\n\treturn fun(x - 1,  x + y);\n}",
        "Which of the following is not O(n^2)?",
        "Which of the following is not object oriented programming?"};
    char *
        options[][4] = {
            {"FlowChart", "Flow", "Algorithm", "Syntex"},
            {"Insertion Sort", "Merge Sort", "Quick Sort", "Bubble Sort"},
            {"Performance", "System Evaluation", "Modularity", "Reliability"},
            {"FlowChart", "Program", "Pseudo Code", " Syntex"},
            {"Stack", "Queue", "Linked List", "Array"},
            {"Linear arrays", "Lists", "Horizontal array", "Vertical array"},
            {"4", "8", "16", "Runtime Error"},
            {"13", "12", "9", "10"},
            {"(15^10) * n + 12099", "n^1.98", "n^3 / (sqrt(n))", "(2^20) * n"},
            {"Python", "Javascirpt", "Java", "C"}};
    char answerkey[] = {3, 3, 1, 2, 2, 1, 3, 1, 3, 4};

    // process the quiz
    printf("You have %d question to solve Each have %d points\n you have four option and select options from 1, 2, 3, 4", totalQuestion, eachPoint);
    for (int i = 0; i < totalQuestion; i++)
    {
        printf("\n%c.%s\n", i + 65, questions[i]);
        printf("1) %s\t2) %s\n3) %s\t4) %s\n", options[i][0], options[i][1], options[i][2], options[i][3]);
        printf("Answer : ");
        int ans;
        scanf("%d", &ans);
        if (ans == answerkey[i])
        {
            printf("You are right! you gain 2 point.");
            totalPoints += eachPoint;
        }
        else
        {
            printf("You are wrong! The right answer is %s.", options[i][answerkey[i] - 1]);
        }
    }
    printf("\nYour total score : %d/%d\n", totalPoints, eachPoint * totalQuestion);
}
